import { Box, Button, Flex, Grid, Text } from "@theme-ui/components";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, {
  FC,
  memo,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import useCart from "../hooks/useCart";
import useOnClickOutside from "../hooks/useOnClickOutside";
import { formatCurrency } from "../utils/format";
import { fromImageToUrl } from "../utils/urls";
import ListEmpty from "./ListEmpty";
import { CardProps } from "./ProductCard";
import OrderPopup from "./OrderPopup";
import { CartIcon, CloseIcon, DeleteIcon } from "../public/icon";
import { useFetchMutation, useFetchQuery } from "../queries";

const Cart: FC = () => {
  const [cart, addToCart, removeToCart, removeAll] = useCart();

  const [isOpenCart, setIsOpenCart] = useState<Boolean>(false);
  const cartRef = useRef<HTMLDivElement | null>(null);
  useOnClickOutside(cartRef, () => {
    setIsOpenCart(false);
  });

  const [isShowPopup, setIsShowPopup] = useState<boolean>(false);
  const popupRef = useRef<HTMLDivElement | null>(null);
  useOnClickOutside(popupRef, () => {
    setIsShowPopup(false);
  });

  const onShowPopup = useCallback(
    () => setIsShowPopup(false),
    [setIsShowPopup]
  );
  const [price, setPrice] = useState<number>(0);
  const [quantity, setQuantity] = useState<number>(0);
  const router = useRouter();
  const { mutate: checkAuth } = useFetchMutation<{ isLogin: boolean }>();
  const { data } = useFetchQuery<CardProps[]>("/products");
  const [products, setProducts] = useState<CardProps[]>([]);

  useEffect(() => {
    setProducts(data?.filter((item) => cart?.find((x) => x.id === item.id)));

    setPrice(
      cart?.reduce((sum, item) => {
        const product = data?.find((x) => x.id === item.id);
        if (product) {
          return sum + product.price * item.quantity;
        }
        return sum;
      }, 0)
    );

    setQuantity(
      cart?.reduce((sum, item) => {
        const product = data?.find((x) => x.id === item.id);
        if (product) {
          return sum + item.quantity;
        }
        return sum;
      }, 0)
    );
  }, [cart, data]);

  const openCart = useCallback(() => setIsOpenCart((prev) => !prev), []);

  const checkOut = useCallback(() => {
    checkAuth("/checkAuth", {
      onSuccess: (data) => {
        if (data.isLogin) {
          router.push("/management", undefined, { shallow: true });
        } else {
          setIsShowPopup(true);
        }
      },
    });
  }, []);

  return (
    <Flex ml="auto">
      <Flex
        onClick={openCart}
        sx={{
          px: 15,
          height: 70,
          width: "100%",
          position: "relative",
          alignItems: "center",
          cursor: "pointer",
          transition: "0.3s",
          ".text": {
            opacity: quantity === 0 ? 0 : 1,
            transform: quantity === 0 ? "scale(0)" : "scale(1)",
          },
          ":hover": {
            ".text": { bg: "productType1" },
            svg: { fill: "productType1", transition: "0.3s" },
          },
        }}
      >
        <Text className="text" p="4px 4px 0px" variant="sumProduct">
          {quantity > 99 ? "99+" : quantity}
        </Text>

        <CartIcon width={30} height={30} />
      </Flex>
      {isOpenCart && (
        <Flex
          sx={{
            position: "fixed",
            bg: "black",
            opacity: 0.4,
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            zIndex: 99,
          }}
        />
      )}

      <Flex
        ref={cartRef}
        sx={{
          position: "fixed",
          flexDirection: "column",
          transition: "0.4s",
          borderRadius: "4px 0 0 4px",
          bg: "white",
          top: 0,
          right: isOpenCart ? 0 : -430,
          width: 375,
          bottom: 0,
          boxShadow: "#959da5 0px 12px 24px 0px",
          zIndex: 100,
        }}
      >
        {isShowPopup && (
          <Flex
            sx={{
              position: "fixed",
              bg: "rgb(137 140 149 / 40%)",
              top: 0,
              right: 0,
              bottom: 0,
              left: 0,
              zIndex: 999999,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Flex ref={popupRef}>
              <OrderPopup
                onShow={onShowPopup}
                type="Notification"
                labelButton="Login"
                content="Please, you have to login to make checkout!"
                href="/login"
              />
            </Flex>
          </Flex>
        )}
        <Grid p={20} variant="cartLabel" columns={"9fr 1fr"}>
          <Flex
            sx={{
              width: "100%",
              flexDirection: "column",
            }}
          >
            <Text variant="cartLabel">IN CART: {quantity} PRODUCT</Text>

            <Text variant="cartLabel1">
              TOTAL PRICE: {formatCurrency(price)}
              <Text as="sup">Đ</Text>
            </Text>
          </Flex>
          <Flex
            mt={3}
            mb={25}
            onClick={openCart}
            sx={{
              justifyContent: "center",
              cursor: "pointer",
              transition: "0.3s",
              ":hover": {
                svg: { fill: "productType1" },
              },
            }}
          >
            <CloseIcon width={15} height={15} />
          </Flex>
        </Grid>
        <Flex
          p={20}
          sx={{
            flexDirection: "column",
            overflowY: "auto",
            "::-webkit-scrollbar": {
              width: 7,
            },
            "::-webkit-scrollbar-thumb": {
              bg: "productType1",
              borderRadius: 5,
            },
          }}
        >
          {products?.length ? (
            products.map((item) => (
              <Flex p="0 9px 20px 0" key={item.id}>
                <Flex
                  sx={{
                    width: 90,
                    height: 90,
                    borderRadius: 5,
                    border: "1px solid #d3d1d1",
                    overflow: "hidden",
                  }}
                >
                  <Box
                    sx={{
                      p: 5,
                      transition: "0.3s",
                      textAlign: "center",
                      alignItems: "center",
                      ":hover": {
                        transform: "scale(1.1)",
                      },
                    }}
                  >
                    <Image
                      src={fromImageToUrl(item.image)}
                      alt="Product"
                      width={76}
                      height={76}
                    />
                  </Box>
                </Flex>
                <Flex
                  ml={12}
                  sx={{
                    flex: 1,
                    flexDirection: "column",
                  }}
                >
                  <Flex
                    sx={{
                      justifyContent: "space-between",
                      svg: {
                        fill: "productType2",
                        transition: "0.3s",
                        cursor: "pointer",
                        ":hover": {
                          fill: "productType1",
                        },
                      },
                    }}
                  >
                    <Link href={`/products/${item.id}`} passHref>
                      <Text variant="cartItemLabel">{item.name}</Text>
                    </Link>
                    <DeleteIcon
                      width={20}
                      height={20}
                      onClick={() => removeAll(item.id)}
                    />
                  </Flex>

                  <Flex
                    sx={{
                      flex: 1,
                      alignItems: "center",
                    }}
                  >
                    <Flex>
                      <Button
                        variant="cartItem"
                        onClick={() => removeToCart(item.id)}
                      >
                        -
                      </Button>
                      <Text variant="cartItemQuantity" my="auto">
                        {cart?.find((x) => x.id === item.id)?.quantity}
                      </Text>
                      <Button
                        variant="cartItem"
                        onClick={() => addToCart(item.id)}
                      >
                        +
                      </Button>
                    </Flex>
                    <Flex
                      sx={{
                        flex: 1,
                        flexDirection: "column",
                        textAlign: "right",
                      }}
                    >
                      {item.priceOld > 0 && (
                        <Text variant="cartItemPriceOld">
                          {formatCurrency(
                            item.priceOld *
                              cart?.find((x) => x.id === item.id)?.quantity
                          )}
                          <Text as="sup">đ</Text>
                        </Text>
                      )}
                      <Text variant="cartItemPrice">
                        {formatCurrency(
                          item.price *
                            cart?.find((x) => x.id === item.id)?.quantity
                        )}
                        <Text as="sup">đ</Text>
                      </Text>
                    </Flex>
                  </Flex>
                </Flex>
              </Flex>
            ))
          ) : (
            <ListEmpty mt={30} />
          )}
        </Flex>
        <Grid columns={2} variant="cartButton">
          <Link href="/products" passHref>
            <Button py={20} variant="primary">
              Shopping
            </Button>
          </Link>
          <Button py={20} variant="secondary" onClick={checkOut}>
            Order
          </Button>
        </Grid>
      </Flex>
    </Flex>
  );
};

export default memo(Cart);
