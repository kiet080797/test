import React, { useState, useEffect, memo } from "react";
import { Flex } from "@theme-ui/components";
import { Arrow } from "../public/icon";

const Scroll = () => {
  const [showScroll, setShowScroll] = useState(false);
  const logit = () => {
    window.pageYOffset >= 150 ? setShowScroll(true) : setShowScroll(false);
  };
  useEffect(() => {
    const watchScroll = () => {
      window.addEventListener("scroll", logit);
    };

    watchScroll();
    return () => {
      window.removeEventListener("scroll", logit);
    };
  });
  return (
    <Flex
      onClick={() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
      }}
      bg="productType1"
      sx={{
        visibility: showScroll ? "visible" : "hidden",
        height: 45,
        width: 45,
        position: "fixed",
        justifyContent: "center",
        alignItems: "center",
        bottom: 15,
        right: 15,
        zIndex: 100,
        transition: "opacity 0.33s ease-in-out 0s",
        borderRadius: 8,
        border: [
          "1px solid rgb(200, 200, 200)",
          "2px solid rgb(200, 200, 200)",
        ],
        cursor: "pointer",
        opacity: 0.6,

        svg: {
          transform: "rotate(-90deg)",
          height: 20,
          width: 20,
          fill: "white",
        },
        ":hover": {
          color: "primary",
          opacity: 1,
          boxShadow: "rgb(0 0 0 / 10%) 0px 0px 12px 0px",
        },
        ":active": {
          transform: "scale(0.95)",
        },
      }}
    >
      <Arrow />
    </Flex>
  );
};

export default memo(Scroll);
