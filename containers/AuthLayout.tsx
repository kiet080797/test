import Head from "next/head";
import React, {
  FC,
  memo,
  PropsWithChildren,
  ReactNode,
  useEffect,
} from "react";
import { Box, Flex, Text } from "theme-ui";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Navigation from "../components/Navigation";
import { useFetchMutation } from "../queries";
import Layout from "./Layout";

const AuthLayout: FC<PropsWithChildren<ReactNode>> = ({ children }) => {
  const { data, mutate: checkAuth } = useFetchMutation<{ isLogin: boolean }>();

  useEffect(() => {
    checkAuth("/checkAuth");
  }, []);

  if (!data?.isLogin) {
    return (
      <Layout>
        <Flex
          sx={{
            height: 300,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text
            sx={{
              mx: 30,
              fontSize: [20, 20, 40, 40, 40],
              lineHeight: "30px",
              fontWeight: 600,
              textTransform: "uppercase",
              letterSpacing: "1px",
              textAlign: "center",
              opacity: 0.7,
              color: "productType2",
            }}
          >
            You must be logged in to access this page!
          </Text>
        </Flex>
      </Layout>
    );
  }
  return (
    <Flex
      bg="white"
      sx={{
        width: "100%",
        flexDirection: "column",
      }}
    >
      <Head>
        <title>Sweet Bakery</title>
        <link rel="shortcut icon" href="/images/favicon.ico" />
        <meta property="og:title" content="Bakery" />
        <meta
          property="og:image"
          content="https://res.cloudinary.com/dtd8ranav/image/upload/v1634816197/bg_hero_21562d49b5.jpg"
        />
        <meta property="og:image:alt" content="bakery" />
        <meta
          name="description"
          content="Specializes in selling all kinds of wholesale and retail cakes"
        />
        <meta
          property="og:description"
          content="Specializes in selling all kinds of wholesale and retail cakes"
        />
      </Head>
      <Header />
      <Navigation />
      <Box>{children}</Box>
      <Footer />
    </Flex>
  );
};

export default memo(AuthLayout);
