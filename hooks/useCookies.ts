export interface userCookies {
  jwt: string;
  name: string;
}

export interface CookiesProps {
  user: userCookies;
}
