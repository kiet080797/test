import "nprogress/nprogress.css";
import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ThemeProvider } from "theme-ui";
import { theme } from "../styles/theme";
import NProgress from "nprogress";
import Router from "next/router";
import { Hydrate, QueryClient, QueryClientProvider } from "react-query";
import { NextPage } from "next";
import { QueryProvider } from "../context/QueryContext";
import ScrollOnTop from "../components/ScrollOnTop";

const queryClient = new QueryClient();

const MyApp: NextPage<AppProps> = ({ Component, pageProps }) => (
  <QueryClientProvider client={queryClient}>
    <Hydrate state={pageProps.dehydratedState}>
      <QueryProvider>
        <ThemeProvider theme={theme}>
          <Component {...pageProps} />
          <ScrollOnTop />
        </ThemeProvider>
      </QueryProvider>
    </Hydrate>
  </QueryClientProvider>
);

NProgress.configure({ showSpinner: false });
Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => {
  NProgress.done();
  window.scrollTo(0, 0);
});
Router.events.on("routeChangeError", () => NProgress.done());

export default MyApp;
