import {
  Box,
  Button,
  Flex,
  Grid,
  Input,
  Label,
  Spinner,
  Text,
  Textarea,
} from "@theme-ui/components";
import Image from "next/image";
import React, { memo, useCallback, useEffect, useRef, useState } from "react";
import { CardProps } from "../../components/ProductCard";
import Layout from "../../containers/Layout";
import { NextPage } from "next";
import { params, useFetchQuery } from "../../queries/fetchData";
import useCart, { ProductLocal } from "../../hooks/useCart";
import { fromImageToUrl } from "../../utils/urls";
import Link from "next/link";
import { formatCurrency } from "../../utils/format";
import ListEmpty from "../../components/ListEmpty";
import { Cycle } from "../../components/ChooseUs";
import { useForm } from "react-hook-form";
import { useOrderMutation } from "../../queries";
import OrderPopup from "../../components/OrderPopup";
import useOnClickOutside from "../../hooks/useOnClickOutside";
import cupcake from "../../public/images/cupcake.png";
import { CartIcon, DeleteIcon, InfoIcon } from "../../public/icon";
import AuthLayout from "../../containers/AuthLayout";
export interface OrderProps {
  phone: string;
  name: string;
  address: string;
  note: string;
  cart: ProductLocal[];
}

const index: NextPage = () => {
  const [cart, addToCart, removeToCart, removeAll, setCart] = useCart();
  const [sumPrice, setSumPrice] = useState<number>(0);
  const { data } = useFetchQuery<CardProps[], params>("/products");
  const { mutate, isLoading } = useOrderMutation();
  const [products, setProducts] = useState<CardProps[]>([]);
  const [sumProduct, setSumProduct] = useState<number>(0);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<OrderProps>({});
  const [success, setSuccess] = useState<boolean>(false);
  const [checkCart, setCheckCart] = useState<boolean>(false);
  const ref = useRef<HTMLDivElement | null>(null);
  const orderSuccess = useRef<HTMLDivElement | null>(null);
  useOnClickOutside(ref, () => {
    setCheckCart(false);
  });
  useOnClickOutside(orderSuccess, () => {
    setSuccess(false);
  });
  const onShow = useCallback(() => setCheckCart(false), [setCheckCart]);
  const onSuccessful = useCallback(() => setSuccess(false), [setSuccess]);

  useEffect(() => {
    setProducts(data?.filter((item) => cart?.find((x) => x.id === item.id)));

    setSumPrice(
      cart?.reduce((sum, item) => {
        const product = data?.find((x) => x.id === item.id);
        if (product) {
          return sum + product.price * item.quantity;
        }
        return sum;
      }, 0)
    );

    setSumProduct(
      cart?.reduce((sum, item) => {
        const product = data?.find((x) => x.id === item.id);
        if (product) {
          return sum + item.quantity;
        }
        return sum;
      }, 0)
    );
  }, [cart, data]);

  const onSubmit = useCallback(
    (data: OrderProps) => {
      if (cart[0]) {
        mutate(data, {
          onSuccess: async () => {
            setSuccess(true);
            setCart([]);
          },
        });
      } else {
        setCheckCart(true);
      }
    },
    [cart]
  );

  register("cart", {
    value: cart,
  });

  return (
    <AuthLayout>
      {checkCart && (
        <Flex
          sx={{
            position: "fixed",
            bg: "rgb(137 140 149 / 40%)",
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            zIndex: 999999,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Flex ref={ref}>
            <OrderPopup
              onShow={onShow}
              type="Notification"
              labelButton="Go Shopping"
              content="You have no products to order!"
              href="/products"
            />
          </Flex>
        </Flex>
      )}
      {success && (
        <Flex
          sx={{
            position: "fixed",
            bg: "rgb(137 140 149 / 40%)",
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            zIndex: 999999,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Flex ref={orderSuccess}>
            <OrderPopup
              onShow={onSuccessful}
              type="Successful"
              labelButton="Go Home"
              content="Congratulations on your successful order!"
              href="/"
            />
          </Flex>
        </Flex>
      )}

      <Flex
        sx={{
          bg: "white",
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Text
          sx={{
            pt: [30, 30, 60, 60, 60],
            pb: [0, 0, 30, 30, 30],
            width: "100%",
            maxWidth: 1170,
            fontSize: [40, 40, 50, 50, 50],
            fontWeight: [700, 700, 600, 600, 600],
            textTransform: "uppercase",
            letterSpacing: "1.5px",
            textAlign: "center",
          }}
        >
          Order page
        </Text>
        <Grid
          columns={[1, 1, "3fr 2fr", "3fr 2fr", "3fr 2fr"]}
          gap={0}
          sx={{
            py: 20,
            width: "100%",
            maxWidth: 1170,
            flexDirection: "column",
          }}
        >
          <Flex
            pt={30}
            sx={{
              flexDirection: "column",
              ":hover": {
                ".icon": {
                  transform: "rotate(-0.15turn)",
                },
              },
            }}
          >
            <Flex
              sx={{
                px: 15,
                alignItems: "center",
              }}
            >
              <Flex
                sx={{
                  position: "relative",
                  justifyContent: "center",
                }}
              >
                <Box
                  sx={{
                    m: "2px 13px 0px",
                    width: 50,
                    pl: 2,
                    svg: { fill: "productType1" },
                  }}
                >
                  <CartIcon width={40} height={40} />
                </Box>
                <Flex
                  className="icon"
                  sx={{
                    position: "absolute",
                    top: -10,
                    transition: "0.3s",
                  }}
                >
                  <Cycle
                    width={70}
                    height={70}
                    opacity={0.07}
                    transform="rotate(90)"
                  />
                </Flex>
              </Flex>
              <Text
                px={15}
                my={15}
                sx={{
                  fontSize: [25, 25, 30, 30, 30],
                  fontWeight: 700,
                  letterSpacing: "1px",
                  color: "productType1",
                }}
              >
                Shopping Cart
              </Text>
            </Flex>

            <Grid
              columns={[
                "6fr 10fr 9fr 3fr",
                "6fr 10fr 9fr 3fr",
                "10fr 8fr 6fr 3fr",
                "10fr 8fr 6fr 3fr",
                "10fr 8fr 6fr 3fr",
              ]}
              gap={[10, 10, 30, 30, 30]}
              p={15}
              mt={15}
              sx={{
                borderBottom: "1px solid #bebebe",
              }}
            >
              <Text variant="cartItemPrice">Product</Text>
              <Text
                variant="cartItemPrice"
                sx={{
                  textAlign: "center",
                }}
              >
                Quantity
              </Text>
              <Text
                variant="cartItemPrice"
                sx={{
                  textAlign: "end",
                }}
              >
                Total
              </Text>
            </Grid>
            <Flex
              px={15}
              sx={{
                maxHeight: 350,
                minHeight: [160, 160, 350, 350, 350],
                flexDirection: "column",
                overflowY: "auto",
                "::-webkit-scrollbar": {
                  width: 7,
                },
                "::-webkit-scrollbar-thumb": {
                  bg: "productType1",
                  borderRadius: 5,
                },
              }}
            >
              {products?.length ? (
                products.map(
                  (item) =>
                    item && (
                      <Grid
                        columns={[
                          "6fr 10fr 9fr 3fr",
                          "6fr 10fr 9fr 3fr",
                          "10fr 8fr 6fr 3fr",
                          "10fr 8fr 6fr 3fr",
                          "10fr 8fr 6fr 3fr",
                        ]}
                        gap={[10, 10, 30, 30, 30]}
                        py={15}
                        key={item?.id}
                        sx={{
                          alignItems: "center",
                          borderBottom: "1px solid #e6e6e6",
                        }}
                      >
                        <Link href={`/products/${item.id}`} passHref>
                          <Grid
                            columns={[
                              "1fr 0",
                              "1fr 0",
                              "50px 1fr",
                              "50px 1fr",
                              "50px 1fr",
                            ]}
                            gap={0}
                            sx={{
                              alignItems: "center",
                            }}
                          >
                            <Flex
                              sx={{
                                width: 50,
                                height: 50,
                                borderRadius: 5,
                                border: "1px solid #d3d1d1",
                                overflow: "hidden",
                              }}
                            >
                              <Box
                                p={2}
                                sx={{
                                  transition: "0.3s",
                                  ":hover": {
                                    transform: "scale(1.1)",
                                  },
                                }}
                              >
                                <Image
                                  src={fromImageToUrl(item.image)}
                                  alt="Product"
                                  width={50}
                                  height={50}
                                />
                              </Box>
                            </Flex>

                            <Text
                              px={15}
                              variant="cartItemLabel"
                              sx={{
                                visibility: [
                                  "hidden",
                                  "hidden",
                                  "visible",
                                  "visible",
                                  "visible",
                                ],
                                whiteSpace: "nowrap",
                                overflowX: "hidden",
                                textOverflow: "ellipsis",
                              }}
                            >
                              {item.name}
                            </Text>
                          </Grid>
                        </Link>
                        <Flex
                          sx={{
                            justifyContent: "center",
                          }}
                        >
                          <Button
                            sx={{
                              width: 30,
                              height: 30,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            variant="cartItem"
                            onClick={() => removeToCart(item.id)}
                          >
                            -
                          </Button>
                          <Text
                            variant="cartItemQuantity"
                            my="auto"
                            sx={{
                              fontSize: 16,
                            }}
                          >
                            {cart?.find((x) => x.id === item.id)?.quantity}
                          </Text>
                          <Button
                            sx={{
                              width: 30,
                              height: 30,
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            variant="cartItem"
                            onClick={() => addToCart(item.id)}
                          >
                            +
                          </Button>
                        </Flex>

                        <Text
                          variant="cartItemPrice"
                          sx={{
                            textAlign: "end",
                            overflowX: "hidden",
                            textOverflow: "ellipsis",
                          }}
                        >
                          {formatCurrency(
                            item.price *
                              cart?.find((x) => x.id === item.id)?.quantity
                          )}
                          <Text as="sup">đ</Text>
                        </Text>

                        <Flex
                          onClick={() => removeAll(item.id)}
                          sx={{
                            justifyContent: "center",
                            width: "100%",
                            height: "100%",
                            alignItems: "center",
                            svg: {
                              fill: "productType2",
                              transition: "0.3s",
                            },
                            cursor: "pointer",
                            ":hover": {
                              svg: { fill: "productType1" },
                            },
                          }}
                        >
                          <DeleteIcon width={15} height={15} />
                        </Flex>
                      </Grid>
                    )
                )
              ) : (
                <Flex
                  sx={{
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  <ListEmpty />
                </Flex>
              )}
            </Flex>
            <Flex>
              <Flex
                sx={{
                  borderTop: "1px solid #d3d1d1",
                  p: 15,
                  width: "100%",
                  flexDirection: "column",
                }}
              >
                <Grid columns={2}>
                  <Text variant="cartLabel">Quantity: </Text>
                  <Text
                    variant="cartLabel"
                    sx={{
                      textAlign: "end",
                    }}
                  >
                    {sumProduct} PRODUCTS
                  </Text>
                </Grid>
                <Grid
                  columns={2}
                  sx={{
                    alignItems: "end",
                  }}
                >
                  <Text variant="cartLabel" color="productType2">
                    Delivery charges:
                  </Text>
                  <Text
                    color="productType2"
                    variant="cartLabel"
                    sx={{
                      textAlign: "end",
                    }}
                  >
                    0<Text as="sup">Đ</Text>
                  </Text>
                </Grid>
                <Grid
                  columns={2}
                  sx={{
                    alignItems: "end",
                  }}
                >
                  <Text
                    variant="cartLabel1"
                    sx={{
                      fontWeight: 700,
                    }}
                  >
                    TOTAL:
                  </Text>
                  <Text
                    variant="cartLabel1"
                    sx={{
                      textAlign: "end",
                      fontWeight: 700,
                    }}
                  >
                    {formatCurrency(sumPrice)}
                    <Text as="sup">Đ</Text>
                  </Text>
                </Grid>
              </Flex>
            </Flex>
          </Flex>
          <Flex
            bg="productType1"
            sx={{
              pt: 30,
              mx: 15,
              borderRadius: 15,
              flexDirection: "column",
              overflowX: "hidden",
              ":hover": {
                ".icon": {
                  transform: "rotate(0.15turn)",
                },
              },
            }}
          >
            <Flex
              sx={{
                px: 15,
                alignItems: "center",
                justifyContent: "center",
                transform: "translateX(15px)",
              }}
            >
              <Text
                px={15}
                my={15}
                sx={{
                  fontSize: [25, 25, 30, 30, 30],
                  fontWeight: 700,
                  letterSpacing: "1px",
                  color: "white",
                }}
              >
                Information
              </Text>
              <Flex
                sx={{
                  position: "relative",
                  justifyContent: "center",
                }}
              >
                <Box
                  sx={{
                    m: "2px 13px 0",
                    width: 50,
                    pl: 2,
                    svg: { fill: "white" },
                  }}
                >
                  <InfoIcon width={40} height={40} />
                </Box>
                <Flex
                  className="icon"
                  sx={{
                    position: "absolute",
                    top: -10,
                    transition: "0.3s",
                  }}
                >
                  <Cycle width={70} height={70} fill="white" opacity={0.15} />
                </Flex>
              </Flex>
            </Flex>
            <Flex
              as="form"
              pt={20}
              sx={{
                flexDirection: "column",
                width: "100%",
                overflow: "hidden",
              }}
              onSubmit={handleSubmit(onSubmit)}
            >
              {/* Name and phone */}
              <Grid columns={2} gap={0}>
                <Box>
                  <Box m="10px 0 10px 20px">
                    <Label
                      sx={{
                        color: "white",
                        letterSpacing: "1px",
                        fontSize: 14,
                        fontWeight: 700,
                      }}
                    >
                      Name
                    </Label>
                    <Input
                      sx={{
                        p: "10px 15px",
                        transition: "0.3s",
                        borderRadius: 30,
                        borderColor: "loginBorder",
                        bg: "white",
                        "::placeholder": {
                          fontSize: 14,
                          opacity: 0.5,
                        },
                        ":focus": {
                          outline: "none",
                          borderColor: "productType5",
                        },
                      }}
                      {...register("name", {
                        required: "Please, enter your name!",
                      })}
                      placeholder="Your name"
                    />
                    {errors.name && (
                      <Text
                        sx={{
                          ml: 15,
                          color: "productType5",
                          fontSize: 14,
                          fontWeight: 600,
                        }}
                      >
                        {errors.name.message}
                      </Text>
                    )}
                  </Box>

                  <Box m="10px 0 10px 20px">
                    <Label
                      sx={{
                        color: "white",
                        letterSpacing: "1px",
                        fontSize: 14,
                        fontWeight: 700,
                      }}
                    >
                      Phone
                    </Label>
                    <Input
                      sx={{
                        p: "10px 15px",
                        transition: "0.3s",
                        borderRadius: 30,
                        borderColor: "loginBorder",
                        bg: "white",
                        "::placeholder": {
                          fontSize: 14,
                          opacity: 0.5,
                        },
                        ":focus": {
                          outline: "none",
                          borderColor: "productType5",
                        },
                      }}
                      {...register("phone", {
                        required: "Please, enter your phone!",
                      })}
                      placeholder="Your phone"
                    />
                    {errors.phone && (
                      <Text
                        sx={{
                          ml: 15,
                          color: "productType5",
                          fontSize: 14,
                          fontWeight: 600,
                        }}
                      >
                        {errors.phone.message}
                      </Text>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    m: "auto",
                    p: 20,
                    width: 150,
                    height: 150,
                    bg: "white",
                    opacity: 0.8,
                    borderRadius: 99,
                    textAlign: "center",
                  }}
                >
                  <Box>
                    <Image
                      src={cupcake}
                      placeholder="blur"
                      width={80}
                      height={80}
                      alt="/images/cupcake.png"
                    />
                  </Box>
                  <Text
                    sx={{
                      fontSize: 18,
                      fontWeight: 700,
                      color: "productType1",
                    }}
                  >
                    Thank you!
                  </Text>
                </Box>
              </Grid>
              {/* Address */}
              <Box m="5px 20px 10px">
                <Label
                  sx={{
                    color: "white",
                    letterSpacing: "1px",
                    fontSize: 14,
                    fontWeight: 700,
                  }}
                >
                  Address
                </Label>
                <Input
                  sx={{
                    p: "10px 15px",
                    transition: "0.3s",
                    borderRadius: 30,
                    borderColor: "loginBorder",
                    bg: "white",
                    "::placeholder": {
                      fontSize: 14,
                      opacity: 0.5,
                    },
                    ":focus": {
                      outline: "none",
                      borderColor: "productType5",
                    },
                  }}
                  {...register("address", {
                    required: "Please, enter your address!",
                  })}
                  placeholder="Address"
                />
                {errors.address && (
                  <Text
                    sx={{
                      ml: 15,
                      color: "productType5",
                      fontSize: 14,
                      fontWeight: 600,
                    }}
                  >
                    {errors.address.message}
                  </Text>
                )}
              </Box>
              {/* Note */}
              <Box m="5px 20px 10px">
                <Label
                  sx={{
                    color: "white",
                    letterSpacing: "1px",
                    fontSize: 14,
                    fontWeight: 700,
                  }}
                >
                  Note
                </Label>
                <Textarea
                  rows={4}
                  sx={{
                    p: "10px 15px",
                    transition: "0.3s",
                    maxHeight: 96,
                    borderRadius: 20,
                    borderColor: "loginBorder",
                    fontSize: 16,
                    fontFamily: "sans-serif",
                    bg: "white",
                    "::placeholder": {
                      fontSize: 14,
                      opacity: 0.5,
                    },
                    ":focus": {
                      outline: "none",
                      borderColor: "productType5",
                    },
                  }}
                  {...register("note")}
                  placeholder="Note"
                />
              </Box>
              {isLoading && (
                <Flex
                  sx={{
                    mt: 10,
                    justifyContent: "center",
                  }}
                >
                  <Spinner color="white" size={35} />
                </Flex>
              )}
              <Button
                disabled={isLoading}
                variant="secondary"
                type="submit"
                sx={{
                  m: 20,
                  py: 20,
                  borderRadius: 999,
                  fontSize: 16,
                  letterSpacing: "2px",
                  bg: "productType1",
                  ":after": {
                    bg: "productType3",
                  },
                }}
              >
                Order
              </Button>
            </Flex>
          </Flex>
        </Grid>
      </Flex>
      <Flex
        sx={{
          py: 35,
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Grid
          columns={"3fr 2fr"}
          gap={0}
          sx={{
            mx: 15,
            width: 1170,
            alignItems: "center",
          }}
        >
          <Text
            px={15}
            sx={{
              color: "productPriceOld",
              letterSpacing: "1px",
              fontSize: [14, 14, 16, 16, 16],
              fontWeight: 500,
              opacity: 0.7,
            }}
          >
            Do you want to continue shopping?
          </Text>
          <Flex>
            <Link href="/products" passHref>
              <Button
                sx={{
                  p: [
                    "15px 20px",
                    "15px 20px",
                    "15px 30px",
                    "15px 30px",
                    "15px 30px",
                  ],
                  borderRadius: 99,
                  fontSize: [12, 12, 16, 16, 16],
                }}
              >
                Go Shopping
              </Button>
            </Link>
          </Flex>
        </Grid>
      </Flex>
    </AuthLayout>
  );
};

export default memo(index);
