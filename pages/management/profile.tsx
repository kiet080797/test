import { Box, Field, Flex, Grid, Spinner, Text } from "@theme-ui/components";
import { NextPage } from "next";
import Link from "next/link";
import React, { memo, useCallback, useState } from "react";
import { Cycle } from "../../components/ChooseUs";
import ListEmpty from "../../components/ListEmpty";
import AuthLayout from "../../containers/AuthLayout";
import { CartIcon, InfoIcon } from "../../public/icon";
import { useFetchQuery, useProfileQuery } from "../../queries";
import { formatCurrency, formatDate } from "../../utils/format";
import { OrderAPIProps } from "../../utils/interface";

const information: NextPage = () => {
  const { data, isLoading } = useFetchQuery<OrderAPIProps[]>("/orders");
  const { data: profile } = useProfileQuery(true);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const onOpen = useCallback(
    (boolean: boolean) => {
      setIsOpen(boolean);
    },
    [setIsOpen]
  );
  return (
    <AuthLayout>
      <Flex
        sx={{
          bg: "white",
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Text
          sx={{
            pt: [30, 30, 60, 60, 60],
            pb: [0, 0, 20, 20, 20],
            width: "100%",
            maxWidth: 1170,
            fontSize: [40, 40, 50, 50, 50],
            fontWeight: 600,
            textTransform: "uppercase",
            letterSpacing: "1.5px",
            textAlign: "center",
          }}
        >
          Management
        </Text>
        <Grid
          columns={[1, 1, "1fr 4fr", "1fr 4fr", "1fr 4fr"]}
          gap={0}
          sx={{
            py: 30,
            width: "100%",
            maxWidth: 1170,
          }}
        >
          <Flex
            sx={{
              mx: 15,
              bg: "productType1",
              pt: [0, 0, 100, 100, 100],
              borderRadius: [
                "20px",
                "20px",
                "20px 0 0 20px",
                "20px 0 0 20px",
                "20px 0 0 20px",
              ],
              border: "1px solid",
              borderColor: "productType1",
              alignItems: "center",
              textAlign: "center",
              flexDirection: ["row", "row", "column", "column", "column"],
            }}
          >
            <Text
              onClick={() => onOpen(false)}
              py={10}
              bg={
                !isOpen && [
                  "productType3",
                  "productType3",
                  "white",
                  "white",
                  "white",
                ]
              }
              sx={{
                width: "100%",
                color: isOpen
                  ? "white"
                  : ["white", "white", "filter", "filter", "filter"],
                borderRadius: [30, 30, 0, 0, 0],
                fontWeight: 700,
                fontSize: 16,
                cursor: "pointer",
                transition: "0.3s",
              }}
            >
              Profile
            </Text>
            <Text
              onClick={() => onOpen(true)}
              py={10}
              bg={
                isOpen && [
                  "productType3",
                  "productType3",
                  "white",
                  "white",
                  "white",
                ]
              }
              sx={{
                width: "100%",
                color: isOpen
                  ? ["white", "white", "filter", "filter", "filter"]
                  : "white",
                borderRadius: [30, 30, 0, 0, 0],
                fontWeight: 700,
                fontSize: 16,
                cursor: "pointer",
                transition: "0.3s",
              }}
            >
              History
            </Text>
          </Flex>
          <Box
            sx={{
              py: 20,
              px: [15, 15, 40, 40, 40],
              ":hover": {
                ".icon": {
                  transform: "rotate(-0.15turn)",
                },
              },
            }}
          >
            <Flex
              sx={{
                height: 80,
                p: "0 0 20px",
                alignItems: "center",
                justifyContent: [
                  "flex-start",
                  "flex-start",
                  "center",
                  "center",
                  "center",
                ],
                borderBottom: !isOpen ? "1px solid" : "none",
                borderBottomColor: "productType1",
              }}
            >
              <Flex
                sx={{
                  position: "relative",
                  justifyContent: "center",
                }}
              >
                <Box
                  sx={{
                    m: "2px 13px 0px",
                    width: 50,
                    pl: 2,
                    svg: { fill: "productType1" },
                  }}
                >
                  {isOpen ? (
                    <CartIcon width={40} height={40} />
                  ) : (
                    <InfoIcon width={40} height={40} />
                  )}
                </Box>
                <Flex
                  className="icon"
                  sx={{
                    position: "absolute",
                    top: -10,
                    transition: "0.3s",
                  }}
                >
                  <Cycle
                    width={70}
                    height={70}
                    opacity={0.07}
                    transform="rotate(90)"
                  />
                </Flex>
              </Flex>
              <Text
                my={15}
                ml={15}
                sx={{
                  fontSize: [22, 22, 28, 28, 28],
                  fontWeight: 700,
                  letterSpacing: "1px",
                  color: "productType1",
                }}
              >
                {isOpen ? "Order History" : "Information"}
              </Text>
            </Flex>

            <Box pt={10}>
              {isOpen ? (
                <Box>
                  <Grid
                    columns={"2fr 4fr 3fr 4fr"}
                    gap={[5, 5, 20, 20, 20]}
                    sx={{
                      pb: 15,
                      textAlign: "center",
                      fontSize: [13, 13, 14, 14, 14],
                      borderBottom: "1px solid",
                      borderBottomColor: "productType1",
                    }}
                  >
                    <Text
                      variant="cartItemPrice"
                      color="productType1"
                      sx={{
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                      }}
                    >
                      No.
                    </Text>
                    <Text variant="cartItemPrice" color="productType1">
                      ID Order
                    </Text>
                    <Text
                      variant="cartItemPrice"
                      sx={{
                        mx: 15,
                        color: "productType1",
                        textAlign: "end",
                      }}
                    >
                      Total
                    </Text>
                    <Text variant="cartItemPrice" color="productType1">
                      Time
                    </Text>
                  </Grid>
                  <Box
                    sx={{
                      height: 437,
                      overflowY: "auto",
                      "::-webkit-scrollbar": {
                        width: 7,
                      },
                      "::-webkit-scrollbar-thumb": {
                        bg: "productType1",
                        borderRadius: 5,
                      },
                    }}
                  >
                    {data?.length ? (
                      data.map((order, index) => (
                        <Link
                          key={order.id}
                          href={`/management/${order.id}`}
                          passHref
                        >
                          <Grid
                            columns={"2fr 4fr 3fr 4fr"}
                            gap={[5, 5, 20, 20, 20]}
                            sx={{
                              py: 5,
                              cursor: "pointer",
                              textAlign: "center",
                              color: "productType2",
                              transition: "0.3s",
                              fontSize: [13, 13, 14, 14, 14],
                              fontWeight: 600,
                              letterSpacing: "0.5px",
                              ":hover": {
                                bg: "productType1",
                                color: "white",
                              },
                            }}
                          >
                            <Text>{index + 1}</Text>
                            <Text
                              sx={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                              }}
                            >
                              {order.id}
                            </Text>
                            <Flex
                              sx={{
                                justifyContent: "end",
                              }}
                            >
                              <Text>{formatCurrency(order.total)}</Text>
                              <Text as="sup">đ</Text>
                            </Flex>

                            <Text
                              sx={{
                                overflow: "hidden",
                                whiteSpace: "nowrap",
                                textOverflow: "ellipsis",
                              }}
                            >
                              {formatDate(order.createdAt)}
                            </Text>
                          </Grid>
                        </Link>
                      ))
                    ) : isLoading ? (
                      <Flex
                        sx={{
                          justifyContent: "center",
                          alignItems: "center",
                          width: "100%",
                          height: "100%",
                        }}
                      >
                        <Spinner my={80} color="productType1" />
                      </Flex>
                    ) : (
                      <Flex
                        sx={{
                          justifyContent: "center",
                          alignItems: "center",
                          width: "100%",
                          height: "100%",
                        }}
                      >
                        <ListEmpty />
                      </Flex>
                    )}
                  </Box>
                </Box>
              ) : (
                <Box>
                  <Field
                    mb={15}
                    sx={{
                      width: "100%",
                      maxWidth: ["100%", "100%", 250, 250, 250],
                      p: "10px 15px",
                      border: "2px solid",
                      borderColor: "productType5",
                    }}
                    defaultValue={profile && profile.firstName}
                    name="First name"
                    label="First name"
                    placeholder="First name"
                    disabled={true}
                  />

                  <Field
                    mb={15}
                    sx={{
                      width: "100%",
                      maxWidth: ["100%", "100%", 250, 250, 250],
                      p: "10px 15px",
                      border: "2px solid",
                      borderColor: "productType5",
                    }}
                    defaultValue={profile && profile.lastName}
                    name="Last name"
                    label="Last name"
                    placeholder="Last name"
                    disabled={true}
                  />

                  <Field
                    mb={15}
                    sx={{
                      width: "100%",
                      maxWidth: 500,
                      p: "10px 15px",
                      border: "2px solid",
                      borderColor: "productType5",
                    }}
                    defaultValue={profile && profile.username}
                    name="Username"
                    label="Username"
                    placeholder="Username"
                    disabled={true}
                  />

                  <Field
                    mb={15}
                    sx={{
                      width: "100%",
                      maxWidth: 500,
                      p: "10px 15px",
                      border: "2px solid",
                      borderColor: "productType5",
                    }}
                    defaultValue={profile && profile.email}
                    name="Email"
                    label="Email"
                    placeholder="Email"
                    disabled={true}
                  />

                  <Field
                    mb={15}
                    sx={{
                      width: "100%",
                      maxWidth: 500,
                      p: "10px 15px",
                      border: "2px solid",
                      borderColor: "productType5",
                    }}
                    defaultValue={profile && profile.email}
                    type="password"
                    name="Password"
                    label="Password"
                    disabled={true}
                    placeholder="Password"
                  />
                  <Text
                    as="i"
                    pt={15}
                    sx={{
                      color: "productType2",
                      opacity: 0.8,
                      fontSize: 14,
                      letterSpacing: "0.5px",
                    }}
                  >
                    * This account was created at{" "}
                    {formatDate(profile && profile.createdAt)}
                  </Text>
                </Box>
              )}
            </Box>
          </Box>
        </Grid>
      </Flex>
    </AuthLayout>
  );
};

export default memo(information);
