import { Flex, Grid, Spinner, Text } from "@theme-ui/components";
import React, { memo, useCallback, useContext } from "react";
import ProductCard, { CardProps } from "../../components/ProductCard";
import Layout from "../../containers/Layout";
import { NextPage } from "next";
import { params, prefetchQuery, useFetchQuery } from "../../queries/fetchData";
import QueryContext from "../../context/QueryContext";
import { useRouter } from "next/router";
import Filter, { SelectItemProps } from "../../components/Filter";
import useHorizontalScroll from "../../hooks/useHorizontalScroll";
import ListEmpty from "../../components/ListEmpty";
import { getStaticPropsFunc } from "../../queries/getStaticProps";

const typeItems: SelectItemProps[] = [
  {
    id: "1",
    label: "All",
    queryName: ["productType"],
    value: ["all"],
  },
  {
    id: "2",
    label: "New",
    queryName: ["productType"],
    value: ["new"],
  },
  {
    id: "3",
    label: "On Sale",
    queryName: ["productType"],
    value: ["sale"],
  },
];

const priceItems: SelectItemProps[] = [
  {
    id: "1",
    label: "All",
    queryName: ["price_lte", "price_gt"],
    value: ["all"],
  },
  {
    id: "2",
    label: "0đ - 50.000đ",
    queryName: ["price_gt", "price_lte"],
    value: ["0", "50000"],
  },
  {
    id: "3",
    label: "50.000đ - 100.000đ",
    queryName: ["price_gt", "price_lte"],
    value: ["50000", "100000"],
  },
  {
    id: "4",
    label: "100.000đ - 200.000đ",
    queryName: ["price_gt", "price_lte"],
    value: ["100000", "200000"],
  },
  {
    id: "5",
    label: " > 200.000đ",
    queryName: ["price_lte", "price_gt"],
    value: ["", "200000"],
  },
];

const typeCakes: SelectItemProps[] = [
  {
    id: "1",
    label: "All",
    queryName: ["cakeType_contains"],
    value: ["all"],
  },
  {
    id: "2",
    label: "Chocolate",
    queryName: ["cakeType_contains"],
    value: ["chocolate"],
  },
  {
    id: "3",
    label: "Truffles",
    queryName: ["cakeType_contains"],
    value: ["truffles"],
  },
  {
    id: "4",
    label: "Cookie",
    queryName: ["cakeType_contains"],
    value: ["cookie"],
  },
];

const index: NextPage = () => {
  const { query } = useContext(QueryContext);
  const { data: products, isLoading } = useFetchQuery<CardProps[], params>(
    "/products",
    query
  );
  const router = useRouter();
  const ref = useHorizontalScroll();
  const onDeleteFilter = useCallback(() => {
    router.push("/products", undefined, { shallow: true });
  }, [router]);

  return (
    <Layout>
      <Flex
        sx={{
          bg: "white",
          width: "100%",
          justifyContent: "center",
        }}
      >
        <Flex
          ref={ref}
          sx={{
            py: 30,
            width: 1170,
            whiteSpace: "nowrap",
            overflowX: "auto",
            "::-webkit-scrollbar": {
              display: "none",
            },
          }}
        >
          <Flex
            sx={{
              minWidth: 700,
              userSelect: "none",
            }}
          >
            <Filter name="Type" selectItem={typeItems} />
            <Filter name="Cake" selectItem={typeCakes} />
            <Filter name="Price" selectItem={priceItems} />
            <Flex>
              <Text
                pr={40}
                variant="filter"
                onClick={onDeleteFilter}
                sx={{
                  color: "productType1",
                  borderColor: "productType1",
                }}
              >
                Delete Filter
              </Text>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      <Flex
        sx={{
          bg: "white",
          width: "100%",
          justifyContent: "center",
        }}
      >
        <Flex
          sx={{
            pb: 60,
            pt: 30,
            width: 1170,
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Text mb={60} variant="offerLabel1">
            PRODUCTS
          </Text>
          {!isLoading ? (
            products?.length ? (
              <Grid gap={0} columns={[1, 2, 2, 3, 4]}>
                {products?.map((item) => (
                  <Flex
                    key={item.id}
                    sx={{
                      p: 15,
                      height: 460,
                    }}
                  >
                    <ProductCard item={item} />
                  </Flex>
                ))}
              </Grid>
            ) : (
              <Flex m="40px 0 80px">
                <ListEmpty />
              </Flex>
            )
          ) : (
            <Spinner my={80} color="productType1" />
          )}
        </Flex>
      </Flex>
    </Layout>
  );
};

export const getStaticProps = getStaticPropsFunc(async ({ queryClient }) => {
  await prefetchQuery(queryClient, "/products");
  return {};
});

export default memo(index);
