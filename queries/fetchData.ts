import {
  QueryClient,
  useMutation,
  useQuery,
  UseQueryOptions,
} from "react-query";
import { ParsedUrlQuery } from "querystring";
import { axiosClient, axiosServer } from ".";
import { OrderProps } from "../pages/management";
import { OrderAPIProps } from "../utils/interface";

export interface params extends ParsedUrlQuery {
  id?: string;
  _limit?: string;
  name_contains?: string;
  id_in?: string[];
}

export async function fetchData<T, P extends {} = {}>(
  url: string,
  variables?: P
): Promise<T> {
  return await axiosClient.get(url, {
    params: variables,
  });
}

export function useFetchQuery<T, P extends {} = {}>(
  url: string,
  variables?: P
) {
  return useQuery([url, ...Object.values(variables ?? {})], () =>
    fetchData<T, P>(url, variables)
  );
}

export function useOrderMutation() {
  async function createOrder(order: OrderProps): Promise<OrderAPIProps> {
    return await axiosClient.post(`orders`, order);
  }
  return useMutation(createOrder);
}

//ssr and ssg
export async function prefetchData<T, P extends {} = {}>(
  url: string,
  variables?: P
): Promise<T> {
  return await axiosServer.get(url, {
    params: variables,
  });
}

export async function prefetchQuery<T, P extends {} = {}>(
  queryClient: QueryClient,
  url: string,
  variables?: P
) {
  await queryClient.prefetchQuery(
    [url, ...Object.values(variables ?? {})],
    () => prefetchData<T, P>(url, variables)
  );
}

export function useFetchMutation<T>() {
  async function fetch(url: string): Promise<T> {
    return await axiosClient.get(url);
  }
  return useMutation(fetch);
}
