import { GetStaticProps, GetStaticPropsContext } from "next";
import { dehydrate, QueryClient } from "react-query";

interface Context extends GetStaticPropsContext {
  queryClient: QueryClient;
}

export const getStaticPropsFunc = <P>(
  fetchProps: (context: Context) => Promise<P>
): GetStaticProps<P> => {
  return async (context: GetStaticPropsContext) => {
    const queryClient = new QueryClient();
    const props = await fetchProps({ ...context, queryClient });
    return {
      props: {
        dehydratedState: dehydrate(queryClient),
        ...props,
      },
      revalidate: 60,
    };
  };
};
