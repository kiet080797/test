export function formatPhoneNumber(phoneNumber: string = "") {
  return `+84${phoneNumber.replace(/^0+/, "")}`;
}

export function formatCurrency(price: number | string = 0) {
  return `${price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`;
}

export function formatDate(date: Date) {
  const weekday = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  const dateFormat = new Date(date);
  return `${weekday[dateFormat.getDay()]}, ${dateFormat.getDate()}-${
    dateFormat.getMonth() + 1
  }-${dateFormat.getFullYear()}`;
}

export const checkActive = (value: string, pathname: string) => {
  return new RegExp(value).test(pathname);
};
