import { CardProps } from "../components/ProductCard";

export interface ProductProps {
  id: number;
  name: string;
  descriptions: string;
  price: string;
  priceOld: number;
  productType: string;
  cakeType: null;
  published_at: Date;
  created_at: Date;
  updated_at: Date;
  image: ImageProps;
  cart_items: CartItem[];
}

export interface CartItem {
  id: string;
  quantity: number;
  product: CardProps;
  order: number;
}

export interface TokenProps {
  id: number;
  username: string;
  lastName: string;
  firstName: string;
  email: string;
  provider: string;
  password: string;
  confirmed: boolean;
  role: number;
  token: number;
  created_at: Date;
  updated_at: Date;
}

export interface OrderAPIProps {
  id: number;
  total: string;
  user: TokenProps;
  status: string;
  name: string;
  phone: string;
  address: string;
  note: string;
  published_at: Date;
  createdAt: Date;
  updated_at: Date;
  cart: CartItem[];
}

export interface ImageProps {
  id: string;
  url: string;
}
