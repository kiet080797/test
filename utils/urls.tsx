import { ImageProps } from "./interface";

export const API_URL = process.env.API_URL;

export const fromImageToUrl = (image: ImageProps) => {
  if (!image) {
    return "/images/empty-bõ.svg";
  }
  return image.url;
};
